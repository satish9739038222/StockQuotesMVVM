//
//  AppDelegate.swift
//  StockQuote
//
//  Created by Satish Kumar on 3/18/18.
//  Copyright © 2018 Satish Kumar. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var viewController: HomePageViewController?
    var navigationcontroller: CustomNavigationViewController?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        viewController = HomePageViewController(nibName: "HomePageViewController", bundle: nil);
        navigationcontroller = CustomNavigationViewController(rootViewController: viewController!);
        navigationcontroller?.navigationItem .setHidesBackButton(true, animated: false);
        navigationcontroller?.navigationBar.isTranslucent = false;
        navigationcontroller?.navigationBar.barTintColor = UIColor(red: 0.141, green: 0.569, blue: 0.788, alpha: 1.0)
        navigationcontroller?.navigationBar.tintColor = UIColor.white;
        navigationcontroller?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "HelveticaNeue-CondensedBold", size: 17)!,NSAttributedStringKey.foregroundColor: UIColor.white]
        window?.rootViewController = navigationcontroller;
        //UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false);
        window?.makeKeyAndVisible();
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

