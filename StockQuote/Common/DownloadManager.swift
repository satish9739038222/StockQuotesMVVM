//
//  DownloadManager.swift
//  StockQuote
//
//  Created by Satish Kumar on 3/19/18.
//  Copyright © 2018 Satish Kumar. All rights reserved.
//

import UIKit

class DownloadManager: NSObject {
    static let sharedInstance = DownloadManager()

    func downloadSymbolPrice(with url: URL?, completionBlock: @escaping (_ succeeded: Bool, _ dataDict: Symbol) -> Void) {
        
        var urlRequest = URLRequest.init(url: url!, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 300)
        urlRequest.httpMethod = "GET"
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        // make the request
        let task = session.dataTask(with: urlRequest) {
            (data, response, error) in
            // check for any errors
            guard error == nil else {
                print("error calling GET on /todos/1")
                print(error!)
                return
            }
            // make sure we got data
            guard let responseData = data else {
                print("Error: did not receive data")
                return
            }
            // parse the result as JSON, since that's what the API provides
            do {
                //var symbols: [Symbol] = []

                if let (json) = try JSONSerialization.jsonObject(with: responseData, options:.mutableContainers) as? [String: Any]{
                    print("Hi")
                            if let symbol = Symbol(json: json) {
                                completionBlock(true,symbol)
                            }
                } else {
                    completionBlock(false,Symbol()!)
                }
                
            } catch  {
                print("error trying to convert data to JSON")
                return
            }
        }
        task.resume()
    }
}
