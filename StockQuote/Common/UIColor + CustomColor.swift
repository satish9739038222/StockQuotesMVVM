//
//  UIColor + CustomColor.swift
//  StockQuote
//
//  Created by Satish Kumar on 3/18/18.
//  Copyright © 2018 Satish Kumar. All rights reserved.
//

import Foundation
import UIKit

extension UIColor{
    
    func titleBarColor()->UIColor{
        let titleBarNavcolor = UIColor(red: 0.141, green: 0.569, blue: 0.788, alpha: 1.0)
        return titleBarNavcolor
    }
    
    func barMaxValueColor()->UIColor{
        let barMaxValueColor = UIColor(red: 0.486, green: 0.678, blue: 0.016, alpha: 1.0)
        return barMaxValueColor
    }
    
    
    func barIntermediateValueColor()->UIColor{
        let barIntermediateValueColor = UIColor(red: 0.216, green: 0.416, blue: 0.596, alpha: 1.0)
        return barIntermediateValueColor
    }
    
    
    func barMinValueColor()->UIColor{
        let titleBarNavcolor = UIColor(red: 0.533, green: 0.596, blue: 0.729, alpha: 1.0)
        return titleBarNavcolor
    }
    
    
    func pieYellowColor()->UIColor{
        let pieYellowColor = UIColor(red: 0.867, green: 0.675, blue: 0.286, alpha: 1.0)
        return pieYellowColor
    }
    
    func pieBlueColor()->UIColor{
        let pieBlueColor = UIColor(red: 0.218, green: 0.388, blue: 0.549, alpha: 1.0)
        return pieBlueColor
    }
    
    func pieGrayColor()->UIColor{
        let pieGrayColor = UIColor(red: 0.502, green: 0.553, blue: 0.675, alpha: 1.0)
        return pieGrayColor
    }
    
    func pieOrangeColor()->UIColor{
        let pieOrangeColor = UIColor(red: 0.937, green: 0.518, blue: 0.180, alpha: 1.0)
        return pieOrangeColor
    }
    
    func pieGreenColor()->UIColor{
        let pieGreenColor = UIColor(red: 0.278, green: 0.529, blue: 0.271, alpha: 1.0)
        return pieGreenColor
    }
    
    
}
