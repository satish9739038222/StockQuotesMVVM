//
//  DetailViewController.swift
//  StockQuote
//
//  Created by Satish Kumar on 3/18/18.
//  Copyright © 2018 Satish Kumar. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    var symbolDetailDisc : [String : Any]!
    @IBOutlet weak var openPrice: UILabel!
    @IBOutlet weak var openTime: UILabel!
    @IBOutlet weak var closePrice: UILabel!
    @IBOutlet weak var closeTime: UILabel!
    @IBOutlet weak var highPrice: UILabel!
    @IBOutlet weak var low: UILabel!
    @IBOutlet weak var latestPrice: UILabel!
    @IBOutlet weak var preClose: UILabel!
    @IBOutlet weak var change: UILabel!
    @IBOutlet weak var changeInPer: UILabel!
    @IBOutlet weak var IEXVol: UILabel!
    @IBOutlet weak var avgTotalVol: UILabel!
    @IBOutlet weak var week52Low: UILabel!
    @IBOutlet weak var week52High: UILabel!

   /* "open":93.01,"openTime":1521552600711,"close":93.13,"closeTime":1521576000272,"high":93.77,"low":93,"latestPrice":93.13,"latestSource":"Close","latestTime":"March 20, 2018","latestUpdate":1521576000272,"latestVolume":23325442,"iexRealtimePrice":93.115,"iexRealtimeSize":16,"iexLastUpdated":1521575999009,"delayedPrice":93.29,"delayedPriceTime":1521579429379,"previousClose":92.89,"change":0.24,"changePercent":0.00258,"iexMarketPercent":0.01639,"iexVolume":382304,"avgTotalVolume":34608527,"iexBidPrice":0,"iexBidSize":0,"iexAskPrice":0,"iexAskSize":0,"marketCap":717081708307,"peRatio":28.22,"week52High":97.24,"week52Low":64.12,"ytdChange":0.0858206250978981}
    */
    override func viewDidLoad() {
        super.viewDidLoad()
        print(symbolDetailDisc)
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
