//
//  Symbol.swift
//  StockQuotes1
//
//  Created by Satish Kumar on 4/12/18.
//  Copyright © 2018 Satish Kumar. All rights reserved.
//

import Foundation

struct Symbol {
    var symbol:String
    var companyName:String
    var latestPrice:NSNumber
    var changePercent:NSNumber
}


extension Symbol {
    init?(json: [String: Any]) {
        guard let symbol = json["symbol"] as? String,
            let companyName = json["companyName"] as? String,
            let latestPrice = json["latestPrice"] as? NSNumber,
            let changePercent = json["changePercent"] as? NSNumber
            else {
                return nil
        }
        
        self.symbol=symbol
        self.companyName=companyName
        self.latestPrice=latestPrice
        self.changePercent=changePercent

    }
    
    init?() {
        self.symbol=""
        self.companyName=""
        self.latestPrice=0
        self.changePercent=0
    }
}


