//
//  HomePageViewController.swift
//  StockQuote
//
//  Created by Satish Kumar on 3/18/18.
//  Copyright © 2018 Satish Kumar. All rights reserved.
//
//https://eodhistoricaldata.com/api/eod/AAPL.US?from=2017-01-05&to=2017-03-10&api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&period=d&fmt=json
//https://eodhistoricaldata.com/api/eod/INTC.US?from=2017-01-05&to=2017-03-10&api_token=OeAFFmMliFG5orCUuwAKQ8l4WWFQ67YX&period=d&fmt=json
//https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=AAPL&interval=1min&apikey=9P8WY4UDZRXZZWFC
//https://www.alphavantage.co/query?function=BATCH_STOCK_QUOTES&symbols=MSFT,FB,AAPL,GOOGL,INTC,IBM,ORCL&apikey=9P8WY4UDZRXZZWFC
//https://www.alphavantage.co/query?function=BATCH_STOCK_QUOTES&symbols=FB,AAPL,ORCL&apikey=9P8WY4UDZRXZZWFC
//https://www.alphavantage.co/query?function=BATCH_STOCK_QUOTES&symbols=IBM&apikey=9P8WY4UDZRXZZWFC
//https://api.iextrading.com/1.0/stock/IBM/quote

//https://api.iextrading.com/1.0/stock/IBM/quote
//https://api.iextrading.com/1.0/stock/AAPL/quote
//https://api.iextrading.com/1.0/stock/GOOGL/quote
//https://api.iextrading.com/1.0/stock/MSFT/quote
//https://api.iextrading.com/1.0/stock/INTC/quote
//https://api.iextrading.com/1.0/stock/ORCL/quote

import UIKit

class HomePageViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    let textCellIdentifier = "HomeTableViewCell"
    let symbolViewModel = SymbolViewModel()

    @IBOutlet weak var homePageTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Stock Quote"

        homePageTableView.delegate = self
        homePageTableView.dataSource = self
        
        let nib = UINib(nibName: "HomeTableViewCell", bundle: nil)
        homePageTableView.register(nib, forCellReuseIdentifier: "HomeTableViewCell")
    
        symbolViewModel.getSymbols {
            self.homePageTableView.reloadData()
        }
    
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (symbolViewModel.arrayToStoreSymbolInfo.count>0){
            return symbolViewModel.arrayToStoreSymbolInfo.count + 1
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        let cell = self.homePageTableView.dequeueReusableCell(withIdentifier: self.textCellIdentifier) as! HomeTableViewCell
        if indexPath.row == 0{
            cell.symbolName.text = "SYMBOL"
            cell.symbolName.frame.origin.y = cell.currentTrade.frame.origin.y
            cell.currentTrade.text = "PRICE"
            cell.deltaInPercentage.text = "DELTA (%)"
            cell.backgroundColor = UIColor(red: 0.941, green: 0.941, blue: 0.941, alpha: 1.0)
            cell.symbolName.font = UIFont(name: "HelveticaNeue-Medium", size: 15.0)
            cell.currentTrade.font = UIFont(name: "HelveticaNeue-Medium", size: 15.0)
            cell.deltaInPercentage.font = UIFont(name: "HelveticaNeue-Medium", size: 15.0)

        }else{
            cell.symbolName.text = symbolViewModel.arrayToStoreSymbolInfo[indexPath.item-1].symbol
            cell.symbolNameDesc.text = symbolViewModel.arrayToStoreSymbolInfo[indexPath.item-1].companyName
            cell.currentTrade.text = (symbolViewModel.arrayToStoreSymbolInfo[indexPath.item-1].latestPrice).stringValue
            cell.deltaInPercentage.text = (symbolViewModel.arrayToStoreSymbolInfo[indexPath.item-1].changePercent).stringValue
            if ((symbolViewModel.arrayToStoreSymbolInfo[indexPath.item-1].changePercent).stringValue.hasPrefix("-")){
                cell.deltaInPercentage.textColor = UIColor.red
            }else{
                cell.deltaInPercentage.textColor = UIColor.green

            }
        }
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0 {
            return 35
        } else {
            return 71
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
            let detailViewController = DetailViewController(nibName: "DetailViewController", bundle: nil)
        //detailViewController.symbolDetailDisc = arrayToStoreSymbolInfo[indexPath.item-1]
            navigationController?.pushViewController(detailViewController, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        return true;
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
