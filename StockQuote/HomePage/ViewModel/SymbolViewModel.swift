//
//  SymbolViewModel.swift
//  StockQuote
//
//  Created by Satish Kumar on 4/17/18.
//  Copyright © 2018 Satish Kumar. All rights reserved.
//

import Foundation

class SymbolViewModel{
    var arrayToStoreSymbolInfo : [Symbol] = []
    
    func getSymbols(completion: @escaping() -> Void) {
        
        for item in Singleton.sharedInstance.arraySymbol{
            DownloadManager.sharedInstance.downloadSymbolPrice(with: URL(string: "https://api.iextrading.com/1.0/stock/\(item)/quote"), completionBlock: { (_ succeeded: Bool, _ dataDict: Symbol) in
                if succeeded {
                    DispatchQueue.main.async {
                        self.arrayToStoreSymbolInfo.append(dataDict)
                        completion()
                    }
                    
                }else{
                    print("Error")
                }
                
            })
        }
    }
}

